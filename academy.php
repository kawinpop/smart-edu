<?php
include 'header.php';
include 'nav-bar.php';
?>

<div id="plan" class="section lb">
    <div class="container">
        <div class="section-title text-center">
            <h3>บรรยากาศในการเรียน</h3>
            <p>เน้นการเรียนสด กับครู รร.รัฐบาลที่มีชื่อเสียง ซึ่งเป็นผู้เชี่ยวชาญในด้านการเรียนการสอน
                ด้วยจำนวนนักเรียนที่เปิดต่อห้องจำนวนไม่เกิน 20 คน
                เพื่อให้นักเรียนสามารถเรียนได้อย่างมีประสิทธิภาพมากที่สุด
                ปรับเนื้อหาการสอนทุกอาทิตย์ โดย รศ.สัญญา รัตนวรารักษ์ เพื่อให้สอดคล้องกับเนื้อหาการเรียนใน
                รร.ของนักเรียน และ การสอบเข้า หรือ การสอบแข่งขัน พร้อมทั้งการวางแผนสอบเข้า ม.1 ให้กับนักเรียนทุกคน
            </p>
        </div><!-- end title -->


        <hr class="invis">

        <div class="row">
            <div class="col-md-12">
                <div class="tab-content">
                    <div class="tab-pane active fade show" id="tab1">
                        <div class="row text-center">
                            <div class="col-md-4">
                                <div class="pricing-table pricing-table-highlighted">
                                    <div class="pricing-table-header grd1">
                                        <h2>ป.2 - ป.3</h2>
                                        <h3>หลักสูตร 9000 บาท/เทอม</h3>
                                    </div>
                                    <div class="pricing-table-space"></div>
                                    <div class="pricing-table-features">
                                        <p><i class="fa fa-envelope-o"></i> <strong>วิชาคณิตศาสตร์</strong></p>
                                        <p><i class="fa fa-rocket"></i> <strong>วิชาภาษาอังกฤษ</strong></p>
                                        <p><i class="fa fa-database"></i> <strong>วิชาวิทยาศาสตร์</strong></p>
                                        <p><i class="fa fa-link"></i> <strong>สามารถผ่อนชำระได้</strong></p>
                                        <p><i class="fa fa-life-ring"></i> <strong>ระยะเวลาเรียน 1 เทอม</strong></p>
                                    </div>
                                    <div class="pricing-table-sign-up">
                                        <a href="#" class="hover-btn-new orange"><span>สมัครเรียน</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="pricing-table pricing-table-highlighted">
                                    <div class="pricing-table-header grd1">
                                        <h2>ป.4 - ป.5</h2>
                                        <h3>วิชาละ 7000 - 8000 บาท/เทอม</h3>
                                    </div>
                                    <div class="pricing-table-space"></div>
                                    <div class="pricing-table-features">
                                        <p><i class="fa fa-envelope-o"></i> <strong>วิชาคณิตศาสตร์เข้มข้น</strong>
                                        </p>
                                        <p><i class="fa fa-rocket"></i> <strong>วิชาภาษาอังกฤษเข้มข้น</strong></p>
                                        <p><i class="fa fa-database"></i> <strong>วิชารวม 5 วิชา</strong></p>
                                        <p><i class="fa fa-link"></i> <strong>สามารถผ่อนชำระได้</strong></p>
                                        <p><i class="fa fa-life-ring"></i> <strong>ระยะเวลาเรียน 1 เทอม</strong></p>
                                    </div>
                                    <div class="pricing-table-sign-up">
                                        <a href="#" class="hover-btn-new orange"><span>สมัครเรียน</span></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="pricing-table pricing-table-highlighted">
                                    <div class="pricing-table-header grd1">
                                        <h2>ป.6</h2>
                                        <h3>วิชาละ 7000 - 8000 บาท/เทอม</h3>
                                    </div>
                                    <div class="pricing-table-space"></div>
                                    <div class="pricing-table-features">
                                        <p><i class="fa fa-envelope-o"></i> <strong>วิชาคณิตศาสตร์เข้มข้น</strong>
                                        </p>
                                        <p><i class="fa fa-rocket"></i> <strong>วิชาภาษาอังกฤษเข้มข้น</strong></p>
                                        <p><i class="fa fa-database"></i> <strong>วิชารวม 5 วิชา</strong></p>
                                        <p><i class="fa fa-link"></i> <strong>สามารถผ่อนชำระได้</strong></p>
                                        <p><i class="fa fa-life-ring"></i> <strong>ระยะเวลาเรียน 1 เทอม</strong></p>
                                    </div>
                                    <div class="pricing-table-sign-up">
                                        <a href="#" class="hover-btn-new orange"><span>สมัครเรียน</span></a>
                                    </div>
                                </div>
                            </div>
                        </div><!-- end row -->
                    </div><!-- end pane -->

                    <div class="tab-pane fade" id="tab2">
                        <div class="row text-center">
                            <div class="col-md-4">
                                <div class="pricing-table pricing-table-highlighted">
                                    <div class="pricing-table-header grd1">
                                        <h2>$477</h2>
                                        <h3>Year</h3>
                                    </div>
                                    <div class="pricing-table-space"></div>
                                    <div class="pricing-table-features">
                                        <p><i class="fa fa-envelope-o"></i> <strong>250</strong> Email Addresses</p>
                                        <p><i class="fa fa-rocket"></i> <strong>125GB</strong> of Storage</p>
                                        <p><i class="fa fa-database"></i> <strong>140</strong> Databases</p>
                                        <p><i class="fa fa-link"></i> <strong>60</strong> Domains</p>
                                        <p><i class="fa fa-life-ring"></i> <strong>24/7 Unlimited</strong> Support
                                        </p>
                                    </div>
                                    <div class="pricing-table-sign-up">
                                        <a href="#" class="hover-btn-new orange"><span>ลงทะเบียน ฟรี!</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="pricing-table pricing-table-highlighted">
                                    <div class="pricing-table-header grd1">
                                        <h2>$485</h2>
                                        <h3>Year</h3>
                                    </div>
                                    <div class="pricing-table-space"></div>
                                    <div class="pricing-table-features">
                                        <p><i class="fa fa-envelope-o"></i> <strong>150</strong> Email Addresses</p>
                                        <p><i class="fa fa-rocket"></i> <strong>65GB</strong> of Storage</p>
                                        <p><i class="fa fa-database"></i> <strong>60</strong> Databases</p>
                                        <p><i class="fa fa-link"></i> <strong>30</strong> Domains</p>
                                        <p><i class="fa fa-life-ring"></i> <strong>24/7 Unlimited</strong> Support
                                        </p>
                                    </div>
                                    <div class="pricing-table-sign-up">
                                        <a href="#" class="hover-btn-new orange"><span>ลงทะเบียน ฟรี!</span></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="pricing-table pricing-table-highlighted">
                                    <div class="pricing-table-header grd1">
                                        <h2>$500</h2>
                                        <h3>Year</h3>
                                    </div>
                                    <div class="pricing-table-space"></div>
                                    <div class="pricing-table-features">
                                        <p><i class="fa fa-envelope-o"></i> <strong>250</strong> Email Addresses</p>
                                        <p><i class="fa fa-rocket"></i> <strong>125GB</strong> of Storage</p>
                                        <p><i class="fa fa-database"></i> <strong>140</strong> Databases</p>
                                        <p><i class="fa fa-link"></i> <strong>60</strong> Domains</p>
                                        <p><i class="fa fa-life-ring"></i> <strong>24/7 Unlimited</strong> Support
                                        </p>
                                    </div>
                                    <div class="pricing-table-sign-up">
                                        <a href="#" class="hover-btn-new orange"><span>ลงทะเบียน ฟรี!</span></a>
                                    </div>
                                </div>
                            </div>
                        </div><!-- end row -->
                    </div><!-- end pane -->
                </div><!-- end content -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</div><!-- end section -->

<?php
include 'footer.php';
include 'script.php';
?>