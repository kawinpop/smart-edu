
<?php
include 'header.php';
include 'nav-bar.php';
?>

    <div class="container bg-white h-100">
      <br><br>
    <h2 style="color:red;" class=" text-center">ประกาศคะแนนสอบเทอม 2/2561</h2>
    <br><br>
        <div class="section-title " id="content">
        <div class="text-center">
             <p>กรุณากรอกเบอร์โทรศัพท์ที่ท่านได้สมัครไว้กับทางโรงเรียน</p>
            <input type="number" class="form-control mt-3" 
            id="phone" name="phone" placeholder="กรุณากรอกเฉพาะตัวเลขเท่านั้น" onkeypress="validate(event)" >
            <button type="button" class="btn btn-success mt-3">คลิก เช็คคะแนนสอบ</button>
        </div>
                
   </div><!-- end title -->

   <div id="load" class="text-center">

   </div>
   
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("button").click(function(){
    if($("#phone").val() === null || $("#phone").val() === ""){
      alert('กรุณากรอกเบอร์โทรศัพท์');
    }else{
      $("#load").html("<h3 style=\"color:blue;\">กำลังดึงข้อมูล...</h3>");
      $.post("test-detail.php",
    {
      phone: $("#phone").val()
    },
    function(data,status){
      $("#load").html("");
      $("#content").html(data);
    });
    }
  
  });
});

  function validate(evt) {
  var theEvent = evt || window.event;

  // Handle paste
  if (theEvent.type === 'paste') {
      key = event.clipboardData.getData('text/plain');
  } else {
  // Handle key press
      var key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode(key);
  }
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}

	</script>

<?php
// include 'footer.php';
include 'script.php';
?>