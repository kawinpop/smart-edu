<footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="widget clearfix">
                        <div class="widget-title">
                            <h3>รร.กวดวิชาครูสัญญา</h3>
                        </div>
                        <p>ห้างตั้งฮั้วเส็ง ธนบุรี ชั้น 5 ใกล้ฟู้ดคอร์ส</p>   
                        <p>289 ถนน สิรินธร แขวง บางพลัด เขต บางพลัด กรุงเทพมหานคร 10700</p> 
                        <p>โทร. 094-462-6229 <br> Email: support@sunya-academy.com</p> 
						<div class="footer-right">
							<ul class="footer-links-soi">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-github"></i></a></li>
							</ul><!-- end links -->
						</div>						
                    </div><!-- end clearfix -->
                </div><!-- end col -->

				<div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="widget clearfix">
                        <div class="widget-title">
                            <h3>แผนผังเว็บไซต์</h3>
                        </div>
                        <ul class="footer-links">
                            <li><a href="index.php">หน้าแรก</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Pricing</a></li>
							<li><a href="#">About</a></li>
							<li><a href="#">Contact</a></li>
                        </ul><!-- end links -->
                    </div><!-- end clearfix -->
                </div><!-- end col -->
				
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="widget clearfix">
                        <div class="widget-title">
                            <h3>ติดต่อเรา</h3>
                        </div>

                        <ul class="footer-links">
                            <li><a href="mailto:#">support@sunya-academy.com</a></li>
                            <li><a href="https://sunya-academy.com">https://sunya-academy.com</a></li>
                            <li>289 ถนน สิรินธร แขวง บางพลัด เขต บางพลัด กรุงเทพมหานคร 10700</li>
                            <li>โทร. 094-462-6229</li>
                        </ul><!-- end links -->
                    </div><!-- end clearfix -->
                </div><!-- end col -->
				
            </div><!-- end row -->
        </div><!-- end container -->
    </footer><!-- end footer -->

    <div class="copyrights">
        <div class="container">
            <div class="footer-distributed">
                <div class="footer-center">                   
                    <p class="footer-company-name">All Rights Reserved. &copy; 2019 <a href="#">Sunya Academy</a> โรงเรียนกวดวิชาครูสัญญา</p>
                </div>
            </div>
        </div><!-- end container -->
    </div><!-- end copyrights -->

    <a href="#" id="scroll-to-top" class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>