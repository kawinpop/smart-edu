<?php
include 'header.php';
include 'nav-bar.php';
?>
<div style="background-color:#fff;">
    <div class="container">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel"
            style="margin-left:-20px;margin-right: -20px;">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="images/slide/1.jpg" class="d-block w-100" alt="คอร์สปิดเทอม เปิดรับสมัครแล้ว">
                </div>
                <div class="carousel-item">
                    <img src="images/slide/2.jpg" class="d-block w-100" alt="คอร์สเปิดเทอม เปิดรับสมัครแล้ว">
                </div>
                <!-- <div class="carousel-item">
      <img src="images/slide/1.jpg" class="d-block w-100" alt="...">
    </div> -->
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>


<div id="overviews" class="section wb">
    <div class="container">
        <div class="section-title row text-center">
            <div class="col-md-12 ">
                <h3>"ความสำเร็จของนักเรียน คือความภูมิใจของครูสัญญา"</h3>
                <p class="lead text-success" style="font-size: 18px;"> <i class="fas fa-award"></i> ลูกๆครูสัญญาสอบติด
                    ม.1 ในปี 2562</p>
            </div>
        </div><!-- end title -->

        <div class="row align-items-center">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                <div class="post-media wow fadeIn">
                    <img src="images/stu_m1/stu_home.jpg" alt="" class="img-fluid img-rounded">
                </div><!-- end media -->
            </div><!-- end col -->
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                <div class="message-box" style="padding: 0px 0px;">
                    <h2>น้องโฮม พิสิฐพงศ์ มุนินทรางกูล</h2>
                    <p class="lead text-primary">สอบได้ที่ 1 โรงเรียนโยธินบูรณะ ห้องเรียนพิเศษ</p>
                    <p>น้องโฮม จาก รร.ธรรมภิรักษ์ เริ่มเรียนกับครูสัญญามาประมาณตั้งแต่ ชั้น ป.4 จนถึงชั้น ป.6
                        และเตรียมตัวและวางแผนสอบเข้า ม.1 กับครูสัญญามาตั้งแต่เนิ่นๆ
                        ด้วยการตั้งเป้าหมายและความแน่วแน่ในการสอบเข้า ม.1
                        น้องโฮมสามารถทำคะแนนทดสอบประเมินผลของคอร์สเรียนของครูสัญญา ได้คะแนนดีขึ้นอย่างต่อเนื่อง
                        และในปัจจุบัน น้องโฮมสามารถสอบติดเข้า ม.1 ตามที่ตั้งใจไว้ และสอบได้เป็นอันดับที่ 1
                        โรงเรียนโยธินบูรณะ ห้องเรียน Gifted
                    </p>
                    <!-- <a href="#" class="hover-btn-new orange"><span>เคล็ดลับสอบโยธิน</span></a> -->
                </div><!-- end messagebox -->
            </div><!-- end col -->


        </div>
        <div class="row align-items-center">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                <div class="post-media wow fadeIn">
                    <img src="images/stu_m1/stu_oom.jpg" alt="" class="img-fluid img-rounded">
                </div><!-- end media -->
            </div><!-- end col -->

            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                <div class="message-box">
                    <h2>น้องโอห์ม ด.ช.ภาสวิชญ์ กุณาศล</h2>
                    <p class="lead text-primary">สอบได้ที่ 1 โรงเรียนวัดนายโรง ห้องเรียน EP</p>
                    <!-- <p>"ครูสัญญา สอนให้ผมรู้จักหลักการในการคิดแก้โจทย์ปัญหาคณิตศาสตร์"</p> -->

                    <p> Integer rutrum ligula eu dignissim laoreet. Pellentesque venenatis nibh sed tellus faucibus
                        bibendum.</p>


                </div><!-- end messagebox -->
            </div><!-- end col -->

        </div><!-- end row -->
        <!-- <div class="text-center">
            <button  class="btn btn-success btn-lg"><span>ลูกๆครูสัญญาสอบติด ม.1 ดูเพิ่มเติม..</span></button>
            </div> -->

    </div><!-- end container -->

</div><!-- end section -->

<section class="section lb page-section">
    <div class="container">
        <div class="section-title row text-center">
            <div class="col-md-12">
                <h3>ส่วนหนึ่งของความสำเร็จ ของลูกๆครูสัญญาในปี 2562</h3>
            </div>
        </div><!-- end title -->
        <div class="timeline">
            <div class="timeline__wrap">
                <div class="timeline__items">
                    <div class="timeline__item">
                        <div class="timeline__content img-bg-01">
                            <h2>น้องโฮม</h2>
                            <p>สอบติด รร.</p>
                        </div>
                    </div>
                    <div class="timeline__item">
                        <div class="timeline__content img-bg-01">
                            <h2>น้อง </h2>
                            <p>สอบติด รร.</p>
                        </div>
                    </div>
                    <div class="timeline__item">
                        <div class="timeline__content img-bg-03">
                            <h2>น้อง </h2>
                            <p>สอบติด รร.</p>
                        </div>
                    </div>
                    <div class="timeline__item">
                        <div class="timeline__content img-bg-02">
                            <h2>น้อง </h2>
                            <p>สอบติด รร.</p>
                        </div>
                    </div>
                    <div class="timeline__item">
                        <div class="timeline__content img-bg-02">
                            <h2>น้อง </h2>
                            <p>สอบติด รร.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- 
	<div class="section cl">
		<div class="container">
			<div class="row text-left stat-wrap">
				<div class="col-md-4 col-sm-4 col-xs-12">
					<span data-scroll class="global-radius icon_wrap effect-1 alignleft"><i class="flaticon-study"></i></span>
					<p>ทดลองเรียนฟรี !</p>
					<h3>จำนวน 2 ชม.</h3>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12">
					<span data-scroll class="global-radius icon_wrap effect-1 alignleft"><i class="flaticon-online"></i></span>
					<p>วางแผนสอบเข้า ม.1</p>
					<h3>รายละเอียดคอร์สเรียน</h3>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12">
					<span data-scroll class="global-radius icon_wrap effect-1 alignleft"><i class="flaticon-years"></i></span>
					<p>ป.6</p>
					<h3>รายละเอียดคอร์สเรียน</h3>
				</div>
			</div>
		</div>
	</div> -->

<div id="overviews" class="section wb">
    <div class="container">
        <div class="section-title row text-center">
            <div class="col-md-12 ">
                <h3>ปรึกษาและวางแผนสอบเข้า ม.1 ฟรี</h3>
                <p class="lead">เพราะการที่จะประสบความสำเร็จได้ ส่วนหนึ่งก็มาจากการกำหนดเป้าหมายที่ชัดเจน  
                โทรปรึกษาเราได้ที่ 094-462-6229
                </p>
            </div>
        </div><!-- end title -->

        <div class="row align-items-center">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                <div class="post-media wow fadeIn">
                    <img src="images/stu_m1/aj_sunya.jpg" alt="" class="img-fluid img-rounded">
                </div><!-- end media -->
            </div><!-- end col -->
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                <div class="message-box" style="padding: 0px 0px;">
                    <h2>ร.ศ.สัญญา รัตนวรารักษ์</h2>
                    <p class="lead text-primary">อดีตผู้ช่วยผู้อำนวยการ อ.คณิตศาสตร์ รร.สาธิตปทุมวัน</p>
                    <p>ครูสัญญา จาก ศูนย์ความรู้สัญญา(SUNYA CENTER) ที่สยามสแควร์ ผู้มีประสบการณ์สอนคณิตศาสตร์มามากกว่า 30 ปี
ปัจจุบันได้ย้ายมาสอนที่ ห้างตั้งฮั้วเส็ง ธนบุรี ชั้น 5
                    </p>
                    <a href="#" class="hover-btn-new orange"><span>ลงทะเบียน วางแผนสอบเข้า ม.1 ฟรี</span></a>
                </div><!-- end messagebox -->
            </div><!-- end col -->
        </div>
    </div><!-- end container -->
</div><!-- end section -->


<div id="plan" class="section lb">
    <div class="container">
        <div class="section-title text-center">
            <h3>บรรยากาศในการเรียน</h3>
            <p>เน้นการเรียนสด กับครู รร.รัฐบาลที่มีชื่อเสียง ซึ่งเป็นผู้เชี่ยวชาญในด้านการเรียนการสอน
                ด้วยจำนวนนักเรียนที่เปิดต่อห้องจำนวนไม่เกิน 20 คน
                เพื่อให้นักเรียนสามารถเรียนได้อย่างมีประสิทธิภาพมากที่สุด
                ปรับเนื้อหาการสอนทุกอาทิตย์ โดย รศ.สัญญา รัตนวรารักษ์ เพื่อให้สอดคล้องกับเนื้อหาการเรียนใน
                รร.ของนักเรียน และ การสอบเข้า หรือ การสอบแข่งขัน พร้อมทั้งการวางแผนสอบเข้า ม.1 ให้กับนักเรียนทุกคน
            </p>
        </div><!-- end title -->

        <div class="main">
  <div class="slider slider-for">
    <!-- <div> <img src="images/slide/pa1.jpg" class="d-block w-100" alt="คอร์สปิดเทอม เปิดรับสมัครแล้ว"></div>
    <div><h3>2</h3></div>
    <div><h3>3</h3></div>
    <div><h3>4</h3></div>
    <div><h3>5</h3></div> -->
  </div>
  <div class="slider slider-nav">
    <div><img src="images/slide/p1.jpg" class="d-block w-100 pr-2" alt="คอร์สปิดเทอม เปิดรับสมัครแล้ว"></div>
    <div><img src="images/slide/p2.jpg" class="d-block w-100 pr-2" alt="คอร์สปิดเทอม เปิดรับสมัครแล้ว"></div>
    <div><img src="images/slide/p3.jpg" class="d-block w-100 pr-2" alt="คอร์สปิดเทอม เปิดรับสมัครแล้ว"></div>
    <div><img src="images/slide/p4.jpg" class="d-block w-100 pr-2" alt="คอร์สปิดเทอม เปิดรับสมัครแล้ว"></div>
    <div><img src="images/slide/p5.jpg" class="d-block w-100 pr-2" alt="คอร์สปิดเทอม เปิดรับสมัครแล้ว"></div>
    <div><img src="images/slide/p6.jpg" class="d-block w-100 pr-2" alt="คอร์สปิดเทอม เปิดรับสมัครแล้ว"></div>
    <div><img src="images/slide/p7.jpg" class="d-block w-100 pr-2" alt="คอร์สปิดเทอม เปิดรับสมัครแล้ว"></div>
  </div>

</div>


    </div><!-- end container -->
</div><!-- end section -->

<div id="testimonials" class="parallax section db parallax-off" style="background-image:url('images/parallax_04.jpg');">
    <div class="container">
        <div class="section-title text-center">
            <h3>รีวิวจากผู้ปกครอง</h3>
            <p>ความคิดเห็นและความรู้สึกที่ได้มาเรียนที่ Sunya Academy</p>
        </div><!-- end title -->

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="testi-carousel owl-carousel owl-theme">
                    <div class="testimonial clearfix">
                        <div class="testi-meta">
                            <img src="images/testi_01.png" alt="" class="img-fluid">
                            <h4>คุณพ่อน้องมิน จาก รร.สารสาสน์เอกตรา</h4>
                        </div>
                        <div class="desc">
                            <h3><i class="fa fa-quote-left"></i> อยากเข้า รร.สามเสนวิทยาลัย ห้อง EP</h3>
                            <iframe
                                src="https://web.facebook.com/plugins/video.php?href=https%3A%2F%2Fweb.facebook.com%2Fsunya.ac%2Fvideos%2F331275557357993%2F&show_text=0&width=560"
                                width="100%" height="230" style="border:none;overflow:hidden" scrolling="no"
                                frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
                            <p class="lead">ปัจจุบันน้องมินสอบติด รร.สามเสนวิทยาลัย ห้อง EP ตามที่ได้ตั้งใจไว้</p>
                        </div>
                        <!-- end testi-meta -->
                    </div>
                    <!-- end testimonial -->

                    <div class="testimonial clearfix">
                        <div class="testi-meta">
                            <img src="images/testi_02.png" alt="" class="img-fluid">
                            <h4>Jacques Philips </h4>
                        </div>
                        <div class="desc">
                            <h3><i class="fa fa-quote-left"></i> Awesome Services!</h3>
                            <p class="lead">Explain to you how all this mistaken idea of denouncing pleasure and
                                praising pain was born and I will give you completed.</p>
                        </div>
                        <!-- end testi-meta -->
                    </div>
                    <!-- end testimonial -->

                    <div class="testimonial clearfix">
                        <div class="testi-meta">
                            <img src="images/testi_03.png" alt="" class="img-fluid ">
                            <h4>Venanda Mercy </h4>
                        </div>
                        <div class="desc">
                            <h3><i class="fa fa-quote-left"></i> Great & Talented Team!</h3>
                            <p class="lead">The master-builder of human happines no one rejects, dislikes avoids
                                pleasure itself, because it is very pursue pleasure. </p>
                        </div>
                        <!-- end testi-meta -->
                    </div>
                    <!-- end testimonial -->
                    <div class="testimonial clearfix">
                        <div class="testi-meta">
                            <img src="images/testi_01.png" alt="" class="img-fluid">
                            <h4>James Fernando </h4>
                        </div>
                        <div class="desc">
                            <h3><i class="fa fa-quote-left"></i> Wonderful Support!</h3>
                            <p class="lead">They have got my project on time with the competition with a sed highly
                                skilled, and experienced & professional team.</p>
                        </div>
                        <!-- end testi-meta -->
                    </div>
                    <!-- end testimonial -->

                    <div class="testimonial clearfix">
                        <div class="testi-meta">
                            <img src="images/testi_02.png" alt="" class="img-fluid">
                            <h4>Jacques Philips </h4>
                        </div>
                        <div class="desc">
                            <h3><i class="fa fa-quote-left"></i> Awesome Services!</h3>
                            <p class="lead">Explain to you how all this mistaken idea of denouncing pleasure and
                                praising pain was born and I will give you completed.</p>
                        </div>
                        <!-- end testi-meta -->
                    </div>
                    <!-- end testimonial -->

                    <div class="testimonial clearfix">
                        <div class="testi-meta">
                            <img src="images/testi_03.png" alt="" class="img-fluid">
                            <h4>Venanda Mercy </h4>
                        </div>
                        <div class="desc">
                            <h3><i class="fa fa-quote-left"></i> Great & Talented Team!</h3>
                            <p class="lead">The master-builder of human happines no one rejects, dislikes avoids
                                pleasure itself, because it is very pursue pleasure. </p>
                        </div>
                        <!-- end testi-meta -->
                    </div><!-- end testimonial -->
                </div><!-- end carousel -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</div><!-- end section -->

<div class="parallax section dbcolor">
    <div class="container">
        <div class="row logos">
            <div class="col-md-6 col-sm-6 col-xs-6 wow fadeInUp">
                <h2 class="text-white">"การเรียนแม้เหนื่อยยาก ยอมลำบากอย่าท้อถอย <br> สุดทางที่รอคอย
                    คืออนาคตอันงดงาม"</h2>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 wow fadeInUp">
                <h2 class="text-white">ปรึกษาและวางแผนสอบเข้า ม.1 ฟรี</h2>
                <h2 class="text-white">โทร. 094-462-6229</h2>
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
</div><!-- end section -->

<?php
include 'footer.php';
include 'script.php';
?>