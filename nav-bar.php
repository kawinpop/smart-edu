<!-- Modal -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header tit-up">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Customer Login</h4>
			</div>
			<div class="modal-body customer-box">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs">
					<li><a class="active" href="#Login" data-toggle="tab">Login</a></li>
					<li><a href="#Registration" data-toggle="tab">Registration</a></li>
				</ul>
				<!-- Tab panes -->
				<div class="tab-content">
					<div class="tab-pane active" id="Login">
						<form role="form" class="form-horizontal">
							<div class="form-group">
								<div class="col-sm-12">
									<input class="form-control" id="email1" placeholder="Name" type="text">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<input class="form-control" id="exampleInputPassword1" placeholder="Email" type="email">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-10">
									<button type="submit" class="btn btn-light btn-radius btn-brd grd1">
										Submit
									</button>
									<a class="for-pwd" href="javascript:;">Forgot your password?</a>
								</div>
							</div>
						</form>
					</div>
					<div class="tab-pane" id="Registration">
						<form role="form" class="form-horizontal">
							<div class="form-group">
								<div class="col-sm-12">
									<input class="form-control" placeholder="Name" type="text">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<input class="form-control" id="email" placeholder="Email" type="email">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<input class="form-control" id="mobile" placeholder="Mobile" type="email">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<input class="form-control" id="password" placeholder="Password" type="password">
								</div>
							</div>
							<div class="row">							
								<div class="col-sm-10">
									<button type="button" class="btn btn-light btn-radius btn-brd grd1">
										Save &amp; Continue
									</button>
									<button type="button" class="btn btn-light btn-radius btn-brd grd1">
										Cancel</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	  </div>
	</div>

    <!-- LOADER -->
	<div id="preloader">
		<div class="loader-container">
			<div class="progress-br float shadow">
				<div class="">
				<img src="images/logo/logo.png" width="60px">
				</div>
			</div>
		</div>
	</div>
	<!-- END LOADER -->	
	
	<!-- Start header -->
	<header class="top-navbar">
		<nav class="navbar navbar-expand-lg navbar-light fixed-top shadow-lg " style="background-color:#fff;">
			<div class="container-fluid shadow">
				<a class="navbar-brand" href="index.php">
					<img src="images/logo.png" class="img-fluid d-none d-lg-block" alt="" style="margin-left: 30%;"/>
					<img src="images/logo.png" class="img-fluid d-lg-none" alt="" style="margin-left: 30%;"/>
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-host" 
				aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation" style="margin-right: 20px;
    background-color: #ccc;">
					<span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbars-host">
					<ul class="navbar-nav ml-auto">
					<?php 
					$url = explode("/",$_SERVER['REQUEST_URI']);
					  ?>
						<li class="nav-item <?php echo $url[2] == 'index.php' ? 'active' : ''; ?>"><a class="nav-link" href="index.php">หน้าแรก</a></li>
						<li class="nav-item <?php echo $url[2] == 'academy.php' ? 'active' : ''; ?>"><a class="nav-link" href="academy.php">คอร์สเรียน</a></li>
						<!-- <li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="dropdown-a" data-toggle="dropdown">คอร์สเรียน </a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
								<a class="dropdown-item" href="course-grid-2.html">Course Grid 2 </a>
								<a class="dropdown-item" href="course-grid-3.html">Course Grid 3 </a>
								<a class="dropdown-item" href="course-grid-4.html">Course Grid 4 </a>
							</div>
						</li> -->
						<li class="nav-item <?php echo $url[2] == 'stu_sunya.php' ? 'active' : ''; ?>"><a class="nav-link" href="stu_sunya.php">นักเรียนปัจจุบัน</a></li>
						<!-- <li class="nav-item dropdown <?php echo $url[2] == 'stu_sunya.php' ? 'active' : ''; ?>">
							<a class="nav-link dropdown-toggle" href="stu_sunya.php" id="dropdown-a" data-toggle="dropdown">นักเรียนปัจจุบัน</a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
								<a class="dropdown-item" href="blog.html">ข่าวการสอบเข้า ม.1 </a>
								<a class="dropdown-item" href="blog-single.html">เทคนิคการเรียน</a>
								<a class="dropdown-item" href="blog-single.html">ทั่วไป</a>
							</div>
						</li> -->
						<li class="nav-item <?php echo $url[2] == 'teacher.php' ? 'active' : ''; ?>"><a class="nav-link" href="teacher.php">Teachers</a></li>
						<li class="nav-item <?php echo $url[2] == 'pricing.php' ? 'active' : ''; ?>"><a class="nav-link" href="pricing.php">อัตตราค่าเรียน</a></li>
						<li class="nav-item <?php echo $url[2] == 'contact.php' ? 'active' : ''; ?>"><a class="nav-link" href="contact.php">ติดต่อเรา</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
                        <li><a class="hover-btn-new log orange" href="#" data-toggle="modal" data-target="#login"><span class="text-white">สมัครเรียน</span></a></li>
                    </ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- End header -->
	<br><br><br>